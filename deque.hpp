#pragma once
#include <cmath>
#include <cstdint>
#include <cstdio>
#include <iostream>
#include <stdexcept>
#include <vector>

template <typename T>
class Deque {
 public:
  template <bool IsConst>
  class common_iterator;

  using iterator = common_iterator<false>;
  using const_iterator = common_iterator<true>;
  using reverse_iterator = std::reverse_iterator<iterator>;
  using const_reverse_iterator = std::reverse_iterator<const_iterator>;

  Deque();
  Deque(const Deque<T>& other);
  explicit Deque(size_t count);
  Deque(size_t count, const T& value);

  Deque<T>& operator=(const Deque<T>& other);
  size_t size() const;
  bool empty() const;

  T& operator[](size_t index);
  const T& operator[](size_t index) const;
  T& at(size_t index);
  const T& at(size_t index) const;

  void push_back(const T& value);
  void push_front(const T& value);
  void pop_back();
  void pop_front();

  iterator begin() const;
  iterator end() const;
  const_iterator cbegin() const;
  const_iterator cend() const;
  reverse_iterator rbegin() const;
  reverse_iterator rend() const;
  const_reverse_iterator crbegin() const;
  const_reverse_iterator crend() const;

  void insert(iterator insert_iter, const T& value);
  void erase(iterator erase_iter);

  ~Deque();

 private:
  void clear_iterators();
  void resize_buckets(size_t new_size_buckets);
  void destruct_elements(iterator begin_elem_it, iterator end_elem_it);
  void free_buckets(size_t begin_buck_ind, size_t end_buck_ind);

  std::vector<T*> buckets_;
  static const ssize_t kSizeBucket = 32;
  iterator begin_it_;
  iterator end_it_;
  /* it is used for maintaining correctness of iterators
   when we extend our memory in the left direction */
  ssize_t shift_ind_for_buckets_ = 0;
};

template <typename T>
Deque<T>::Deque() : begin_it_(this, 0, 0), end_it_(this, 0, 0) {}

template <typename T>
Deque<T>::Deque(const Deque<T>& other) : Deque() {
  resize_buckets(other.buckets_.size());

  begin_it_.assign_indexes(other.begin_it_);
  end_it_.assign_indexes(other.end_it_);
  shift_ind_for_buckets_ = other.shift_ind_for_buckets_;

  iterator cur_it = begin();
  for (const_iterator other_it = other.cbegin(); other_it != other.cend();
       ++other_it, ++cur_it) {
    try {
      new (cur_it.operator->()) T(*other_it);
    } catch (...) {  // return object to the initial state
      destruct_elements(begin(), cur_it);
      free_buckets(0, buckets_.size());

      buckets_.resize(0);
      clear_iterators();
      shift_ind_for_buckets_ = 0;

      std::cerr
          << "Deque(const Deque<T>& other): exception in placement new!\n";
      throw;
    }
  }
}

template <typename T>
Deque<T>::Deque(size_t count) : Deque() {
  size_t count_buckets = 2 * ((count - 1) / kSizeBucket + 1);
  resize_buckets(count_buckets);

  iterator cur_it = begin();
  for (size_t i = 0; i < count; ++i, ++cur_it) {
    try {
      new (cur_it.operator->()) T();
    } catch (...) {
      destruct_elements(begin(), cur_it);
      free_buckets(0, buckets_.size());

      buckets_.resize(0);
      clear_iterators();
      shift_ind_for_buckets_ = 0;

      std::cerr << "Deque(size_t): exception in placement new!\n";
      throw;
    }
  }
  end_it_ = cur_it;
}

template <typename T>
Deque<T>::Deque(size_t count, const T& value) : Deque() {
  size_t count_buckets = 2 * ((count - 1) / kSizeBucket + 1);
  resize_buckets(count_buckets);

  iterator cur_it = begin();
  for (size_t i = 0; i < count; ++i, ++cur_it) {
    try {
      new (cur_it.operator->()) T(value);
    } catch (...) {
      destruct_elements(begin(), cur_it);
      free_buckets(0, buckets_.size());

      buckets_.resize(0);
      clear_iterators();
      shift_ind_for_buckets_ = 0;

      std::cerr << "Deque(size_t, const T&): exception in placement new!\n";
      throw;
    }
  }
  end_it_ = cur_it;
}

template <typename T>
Deque<T>& Deque<T>::operator=(const Deque<T>& other) {
  // save object for returning to the initial state if we get an exception
  Deque<T> copy(*this);
  // clear object
  destruct_elements(begin_it_, end_it_);
  if (other.buckets_.size() > buckets_.size()) {
    resize_buckets(other.buckets_.size());
  }
  clear_iterators();
  shift_ind_for_buckets_ = 0;
  // copy elements from other
  for (const_iterator other_it = other.cbegin(); other_it != other.cend();
       ++other_it, ++end_it_) {
    try {
      new (end_it_.operator->()) T(*other_it);
    } catch (...) {
      // destruct elements that were copied from other
      destruct_elements(begin(), end_it_);
      // restore the data from copy
      shift_ind_for_buckets_ = copy.shift_ind_for_buckets_;
      begin_it_.assign_indexes(copy.begin_it_);
      end_it_.assign_indexes(copy.end_it_);

      for (iterator it = begin_it_, copy_it = copy.begin_it_; it != end_it_;
           ++it, ++copy_it) {
        new (it.operator->()) T(*copy_it);
      }

      perror("exception in operator= of deque!");
      throw;
    }
  }
  return *this;
}

template <typename T>
size_t Deque<T>::size() const {
  return static_cast<size_t>(end_it_ - begin_it_);
}

template <typename T>
bool Deque<T>::empty() const {
  return size() == 0;
}

template <typename T>
T& Deque<T>::operator[](size_t index) {
  return *(begin_it_ + index);
}

template <typename T>
const T& Deque<T>::operator[](size_t index) const {
  return *(begin_it_ + index);
}

template <typename T>
T& Deque<T>::at(size_t index) {
  if (index >= size()) {
    throw std::out_of_range("bad index for at");
  }
  return *(begin_it_ + index);
}

template <typename T>
const T& Deque<T>::at(size_t index) const {
  if (index >= size()) {
    throw std::out_of_range("bad index for at");
  }
  return *(begin_it_ + index);
}

template <typename T>
void Deque<T>::push_back(const T& value) {
  if ((end_it_.buck_ind_ + shift_ind_for_buckets_ ==
           static_cast<ssize_t>(buckets_.size()) &&
       end_it_.elem_ind_ > 0) ||
      end_it_.buck_ind_ + shift_ind_for_buckets_ >
          static_cast<ssize_t>(buckets_.size())) {
    std::cerr << "wrong end() in Deque!\n";
    throw;
  }
  // extend amount of buckets if we don't have enough memory
  if (end_it_.buck_ind_ + shift_ind_for_buckets_ ==
          static_cast<ssize_t>(buckets_.size()) &&
      end_it_.elem_ind_ == 0) {
    if (!buckets_.empty()) {  // check emptiness buckets for correct resize
      resize_buckets(2 * buckets_.size());
    } else {
      resize_buckets(2);
    }
  }

  new (end_it_.operator->()) T(value);
  ++end_it_;
}

template <typename T>
void Deque<T>::push_front(const T& value) {
  if ((begin_it_.buck_ind_ + shift_ind_for_buckets_ < 0)) {
    std::cerr << "wrong begin() in Deque!\n";
    throw;
  }
  if (begin_it_.buck_ind_ + shift_ind_for_buckets_ == 0 &&
      begin_it_.elem_ind_ == 0) {
    if (!buckets_.empty()) {
      // extend buckets in the right direction
      size_t old_bucket_size = buckets_.size();
      resize_buckets(2 * buckets_.size());
      /* swap halves of buckets for getting extension buckets
       in the left direction */
      for (size_t i = 0; i < old_bucket_size; ++i) {
        std::swap(buckets_[i], buckets_[i + old_bucket_size]);
      }
      // make shift for saving correctness of iterators
      shift_ind_for_buckets_ += old_bucket_size;
    } else {
      resize_buckets(2);
      shift_ind_for_buckets_ = 1;
    }
  }

  --begin_it_;
  new (begin_it_.operator->()) T(value);
}

template <typename T>
void Deque<T>::pop_back() {
  if ((empty())) {
    std::cerr << "empty Deque in pop_back()!\n";
    throw;
  }
  if (!empty()) {
    --end_it_;
    end_it_->~T();
  }
}

template <typename T>
void Deque<T>::pop_front() {
  if ((empty())) {
    std::cerr << "empty Deque in pop_front()!\n";
    throw;
  }
  if (!empty()) {
    begin_it_->~T();
    ++begin_it_;
  }
}

template <typename T>
typename Deque<T>::iterator Deque<T>::begin() const {
  return begin_it_;
}

template <typename T>
typename Deque<T>::iterator Deque<T>::end() const {
  return end_it_;
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::cbegin() const {
  return static_cast<const_iterator>(begin_it_);
}

template <typename T>
typename Deque<T>::const_iterator Deque<T>::cend() const {
  return static_cast<const_iterator>(end_it_);
}

template <typename T>
typename Deque<T>::reverse_iterator Deque<T>::rbegin() const {
  return std::make_reverse_iterator(end_it_);
}

template <typename T>
typename Deque<T>::reverse_iterator Deque<T>::rend() const {
  return std::make_reverse_iterator(begin_it_);
}

template <typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crbegin() const {
  return std::make_reverse_iterator(static_cast<const_iterator>(end_it_));
}

template <typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crend() const {
  return std::make_reverse_iterator(static_cast<const_iterator>(begin_it_));
}

template <typename T>
void Deque<T>::insert(Deque<T>::iterator insert_iter, const T& value) {
  if ((end_it_.buck_ind_ + shift_ind_for_buckets_ ==
           static_cast<ssize_t>(buckets_.size()) &&
       end_it_.elem_ind_ > 0) ||
      end_it_.buck_ind_ + shift_ind_for_buckets_ >
          static_cast<ssize_t>(buckets_.size())) {
    std::cerr << "insert: wrong end() in Deque\n";
    throw;
  }
  // push_back if we insert in the end of deque
  if (insert_iter == end_it_) {
    push_back(value);
    return;
  }
  // if we don't have enough buckets then extend memory in the right direction
  if (end_it_.buck_ind_ + shift_ind_for_buckets_ ==
          static_cast<ssize_t>(buckets_.size()) &&
      end_it_.elem_ind_ == 0) {
    if (!buckets_.empty()) {
      resize_buckets(2 * buckets_.size());
    } else {
      resize_buckets(2);
    }
  }
  // shift elements to the right
  new (end_it_.operator->()) T(*(end_it_ - 1));
  for (iterator it = end_it_ - 1; it != insert_iter; --it) {
    *it = *(it - 1);
  }

  *insert_iter = value;
  ++end_it_;
}

template <typename T>
void Deque<T>::erase(Deque<T>::iterator erase_iter) {
  if (erase_iter == end_it_ - 1) {
    pop_back();
    return;
  }

  erase_iter->~T();
  // shift elements to the left
  new (erase_iter.operator->()) T(*(erase_iter + 1));
  for (iterator it = erase_iter + 1; it != end_it_ - 1; ++it) {
    *it = *(it + 1);
  }
  --end_it_;
}

template <typename T>
Deque<T>::~Deque() {
  destruct_elements(begin_it_, end_it_);
  free_buckets(0, buckets_.size());
}

template <typename T>
void Deque<T>::clear_iterators() {
  begin_it_.buck_ind_ = 0;
  begin_it_.elem_ind_ = 0;
  end_it_.buck_ind_ = 0;
  end_it_.elem_ind_ = 0;
}

template <typename T>
void Deque<T>::resize_buckets(size_t new_size_buckets) {
  if (new_size_buckets >= buckets_.size()) {
    size_t old_size = buckets_.size();
    size_t ind = old_size;
    try {
      buckets_.resize(new_size_buckets);
      for (; ind < new_size_buckets; ++ind) {
        buckets_[ind] =
            reinterpret_cast<T*>(new int8_t[kSizeBucket * sizeof(T)]);
      }
    } catch (std::bad_alloc&) {  // return to the initial state
      free_buckets(old_size, ind);
      buckets_.resize(old_size);
      std::cerr << "resize_buckets: bad_alloc!\n";
      throw;
    }
  } else {
    free_buckets(new_size_buckets, buckets_.size());
    buckets_.resize(new_size_buckets);
  }
}

template <typename T>
void Deque<T>::destruct_elements(iterator begin_elem_it, iterator end_elem_it) {
  for (iterator it = begin_elem_it; it != end_elem_it; ++it) {
    (buckets_[it.buck_ind_ + shift_ind_for_buckets_] + it.elem_ind_)->~T();
  }
}

template <typename T>
void Deque<T>::free_buckets(size_t begin_buck_ind, size_t end_buck_ind) {
  for (size_t i = begin_buck_ind; i < end_buck_ind; ++i) {
    delete[] reinterpret_cast<int8_t*>(buckets_[i]);
  }
}

// iterator

template <typename T>
template <bool IsConst>
class Deque<T>::common_iterator {
  friend Deque<T>;
  friend Deque<T>::common_iterator<true>;

 public:
  using type = std::conditional_t<IsConst, const T, T>;

  using iterator_category = std::random_access_iterator_tag;
  using value_type = type;
  using pointer = type*;
  using reference = type&;
  using difference_type = ssize_t;

  common_iterator() : deq_(nullptr), buck_ind_(0), elem_ind_(0){};
  common_iterator(Deque<T>* deq, ssize_t bucket_ind, ssize_t element_ind);
  // we can construct const_iterator only from const_iterator
  common_iterator(const common_iterator<true>& other) requires(IsConst)
      : common_iterator(other.deq_, other.buck_ind_, other.elem_ind_) {}
  common_iterator(const common_iterator<false>& other)
      : common_iterator(other.deq_, other.buck_ind_, other.elem_ind_) {}
  common_iterator<true>& operator=(const common_iterator<true>& other) {
    deq_ = other.deq_;
    buck_ind_ = other.buck_ind_;
    elem_ind_ = other.elem_ind_;
    return *this;
  }
  common_iterator& operator=(const common_iterator<false>& other) {
    deq_ = other.deq_;
    buck_ind_ = other.buck_ind_;
    elem_ind_ = other.elem_ind_;
    return *this;
  }

  common_iterator<IsConst>& operator++();
  common_iterator<IsConst> operator++(int);
  common_iterator<IsConst>& operator--();
  common_iterator<IsConst> operator--(int);
  common_iterator<IsConst>& operator+=(ssize_t shift);
  common_iterator<IsConst> operator+(ssize_t shift) const;
  common_iterator<IsConst>& operator-=(ssize_t shift);
  common_iterator<IsConst> operator-(ssize_t shift) const;
  ssize_t operator-(const common_iterator<IsConst>& other) const;

  bool operator<(const common_iterator& other) const;
  bool operator>(const common_iterator& other) const;
  bool operator<=(const common_iterator& other) const;
  bool operator>=(const common_iterator& other) const;
  bool operator==(const common_iterator& other) const;
  bool operator!=(const common_iterator& other) const;

  type& operator*();
  type operator*() const;
  type* operator->() const;

 private:
  int compare_iterators(const common_iterator& other) const;
  void assign_indexes(const common_iterator& other);

  Deque<T>* deq_;
  ssize_t buck_ind_;
  ssize_t elem_ind_;
};

template <typename T>
template <bool IsConst>
Deque<T>::common_iterator<IsConst>::common_iterator(Deque<T>* deq,
                                                    ssize_t bucket_ind,
                                                    ssize_t element_ind)
    : deq_(deq), buck_ind_(bucket_ind), elem_ind_(element_ind) {}

template <typename T>
template <bool IsConst>
typename Deque<T>::template common_iterator<IsConst>&
Deque<T>::common_iterator<IsConst>::operator++() {
  if (elem_ind_ + 1 == deq_->kSizeBucket) {
    ++buck_ind_;
    elem_ind_ = 0;
    return *this;
  }
  ++elem_ind_;
  return *this;
}

template <typename T>
template <bool IsConst>
typename Deque<T>::template common_iterator<IsConst>
Deque<T>::common_iterator<IsConst>::operator++(int) {
  Deque<T>::common_iterator<IsConst> copy = *this;
  if (elem_ind_ + 1 == deq_->kSizeBucket) {
    ++buck_ind_;
    elem_ind_ = 0;
    return copy;
  }
  ++elem_ind_;
  return copy;
}

template <typename T>
template <bool IsConst>
typename Deque<T>::template common_iterator<IsConst>&
Deque<T>::common_iterator<IsConst>::operator--() {
  if (elem_ind_ == 0) {
    --buck_ind_;
    elem_ind_ = deq_->kSizeBucket - 1;
    return *this;
  }
  --elem_ind_;
  return *this;
}

template <typename T>
template <bool IsConst>
typename Deque<T>::template common_iterator<IsConst>
Deque<T>::common_iterator<IsConst>::operator--(int) {
  Deque<T>::common_iterator<IsConst> copy = *this;
  if (elem_ind_ == 0) {
    --buck_ind_;
    elem_ind_ = deq_->kSizeBucket - 1;
    return *copy;
  }
  --elem_ind_;
  return *copy;
}

template <typename T>
template <bool IsConst>
typename Deque<T>::template common_iterator<IsConst>&
Deque<T>::common_iterator<IsConst>::operator+=(ssize_t shift) {
  buck_ind_ += shift / kSizeBucket;
  shift -= (shift / kSizeBucket) * kSizeBucket;

  elem_ind_ += shift;
  if (elem_ind_ < 0) {
    elem_ind_ += kSizeBucket;
    --buck_ind_;
    return *this;
  }
  if (elem_ind_ / kSizeBucket == 1) {
    elem_ind_ %= kSizeBucket;
    ++buck_ind_;
  }
  return *this;
}

template <typename T>
template <bool IsConst>
typename Deque<T>::template common_iterator<IsConst>
Deque<T>::common_iterator<IsConst>::operator+(ssize_t shift) const {
  Deque<T>::common_iterator<IsConst> copy = *this;
  copy += shift;
  return copy;
}

template <typename T>
template <bool IsConst>
typename Deque<T>::template common_iterator<IsConst>&
Deque<T>::common_iterator<IsConst>::operator-=(ssize_t shift) {
  shift = -shift;
  return (*this += shift);
}

template <typename T>
template <bool IsConst>
typename Deque<T>::template common_iterator<IsConst>
Deque<T>::common_iterator<IsConst>::operator-(ssize_t shift) const {
  shift = -shift;
  return (*this + shift);
}

template <typename T>
template <bool IsConst>
ssize_t Deque<T>::common_iterator<IsConst>::operator-(
    const common_iterator<IsConst>& other) const {
  if (deq_ == other.deq_) {
    return (buck_ind_ - other.buck_ind_) * kSizeBucket +
           (elem_ind_ - other.elem_ind_);
  }
  std::cerr << "different deques in common_iterator::operator-!\n";
  throw;
}

template <typename T>
template <bool IsConst>
bool Deque<T>::common_iterator<IsConst>::operator<(
    const common_iterator& other) const {
  return compare_iterators(other) < 0;
}

template <typename T>
template <bool IsConst>
bool Deque<T>::common_iterator<IsConst>::operator>(
    const common_iterator& other) const {
  return compare_iterators(other) > 0;
}

template <typename T>
template <bool IsConst>
bool Deque<T>::common_iterator<IsConst>::operator<=(
    const common_iterator& other) const {
  return compare_iterators(other) <= 0;
}

template <typename T>
template <bool IsConst>
bool Deque<T>::common_iterator<IsConst>::operator>=(
    const common_iterator& other) const {
  return compare_iterators(other) >= 0;
}

template <typename T>
template <bool IsConst>
bool Deque<T>::common_iterator<IsConst>::operator==(
    const common_iterator& other) const {
  return compare_iterators(other) == 0;
}

template <typename T>
template <bool IsConst>
bool Deque<T>::common_iterator<IsConst>::operator!=(
    const common_iterator& other) const {
  return compare_iterators(other) != 0;
}

template <typename T>
template <bool IsConst>
typename Deque<T>::template common_iterator<IsConst>::type&
Deque<T>::common_iterator<IsConst>::operator*() {
  return *(deq_->buckets_[buck_ind_ + deq_->shift_ind_for_buckets_] +
           elem_ind_);
}

template <typename T>
template <bool IsConst>
typename Deque<T>::template common_iterator<IsConst>::type
Deque<T>::common_iterator<IsConst>::operator*() const {
  return *(deq_->buckets_[buck_ind_ + deq_->shift_ind_for_buckets_] +
           elem_ind_);
}

template <typename T>
template <bool IsConst>
typename Deque<T>::template common_iterator<IsConst>::type*
Deque<T>::common_iterator<IsConst>::operator->() const {
  return deq_->buckets_[buck_ind_ + deq_->shift_ind_for_buckets_] + elem_ind_;
}

template <typename T>
template <bool IsConst>
int Deque<T>::common_iterator<IsConst>::compare_iterators(
    const common_iterator& other) const {
  if (deq_ == other.deq_) {
    if (buck_ind_ < other.buck_ind_) {
      return -1;
    }
    if (buck_ind_ > other.buck_ind_) {
      return 1;
    }
    if (elem_ind_ < other.elem_ind_) {
      return -1;
    }
    if (elem_ind_ > other.elem_ind_) {
      return 1;
    }
    return 0;
  }
  std::cerr << "different deques in compare!\n";
  throw;
}

template <typename T>
template <bool IsConst>
void Deque<T>::common_iterator<IsConst>::assign_indexes(
    const common_iterator& other) {
  buck_ind_ = other.buck_ind_;
  elem_ind_ = other.elem_ind_;
}